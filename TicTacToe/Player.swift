//
//  Player.swift
//  TicTacToe
//
//  Created by Rodolphe Cargnello on 09/04/2016.
//  Copyright © 2016 Rodolphe Cargnello. All rights reserved.
//

import Foundation

protocol Player {
	func play(ticTacToe: TicTacToe) -> (Int,Int)
}
// Copyright © 2016 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation

class Tree<T> {
	
	var data: T?
	var children: Array<Tree<T>>
	
	init(){
		children = []
	}
	
	init(data: T){
		self.data = data
		children = []
	}
	
	init(tree: Tree<T>){
		data = tree.data
		children = tree.children
	}
	
	func nbChildren() -> Int {
		var nb = children.count;
		for t in children{
			nb += t.nbChildren();
		}
		return nb;
	}
	
}

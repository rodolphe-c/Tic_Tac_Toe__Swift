// Copyright © 2016 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation

// Player artificial intelligence

class PlayerAI: Player {
	
	// Play
	
	func play(ticTacToe: TicTacToe) throws -> (Int,Int) {
		let begin = NSDate()
		
		var games = Tree<(game: TicTacToe, score: Int)>(data: (ticTacToe, 0))
		try generateChildren(games);
		minimax(&games, is_playing: ticTacToe.isPlaying())
		
		let end = NSDate()
		
		print("\(games.nbChildren()) children generated in  \(end.timeIntervalSinceDate(begin)) seconds\n")
		
		if (games.children.count == 0){
			throw PlayerError.InvalidSelection(error: "ERROR: PlayerAI.play: no child :(");
		}else{
			for e in games.children{
				print("\(e.data!.game.logPlays()[e.data!.game.logPlays().count-1]) - \(e.data!.score)\n")
			}
			
			var r = TicTacToe()
			var max = Int.min
			for child in games.children{
				if (child.data!.score > max)
				{
					max = child.data!.score
					r = child.data!.game
				}
			}
			
			return r.logPlays()[r.logPlays().count-1]
		}
	}
	
	// Generate games (children) for one tic_tac_toe (one level only)
	
	private func generateGames(inout ticTacToe: TicTacToe) throws -> Array<Tree<(game: TicTacToe, score: Int)>>{
		var moves = ticTacToe.movesPossible();
	
		var r = Array<Tree<(game: TicTacToe, score: Int)>>()
		
		for i in 0 ..< moves.count{
			r.insert(Tree<(game: TicTacToe, score: Int)>(data: (ticTacToe.copy(), 0)), atIndex: r.endIndex)
			try r[i].data!.game.play(moves[i])
		}
	
		return r
	}
	
	// Generate all children and their children and so on
	
	private func generateChildren(games: Tree<(game: TicTacToe, score: Int)>) throws{
		try games.children = generateGames(&games.data!.game);
	
		for child in games.children{
			try generateChildren(child);
		}
	}
	
	// Evaluate games (nodes)
	
	private func minimax(inout games: Tree<(game: TicTacToe, score: Int)> , is_playing: Tile){
		if (games.data!.game.isEnded()){
			if games.data!.game.winner() != Tile.Empty{
				if games.data!.game.winner() == is_playing { games.data!.score = 1 }
				else { games.data!.score = -1; }
			}
		}else{
			for var child in games.children{
				minimax(&child, is_playing: is_playing);
			}
			
			var min = games.children[0].data!.score
			var max = games.children[0].data!.score
			for value in games.children[1..<games.children.count] {
				if value.data!.score < min {
					min = value.data!.score
				} else if value.data!.score > max {
					max = value.data!.score
				}
			}
	
			if games.data!.game.isPlaying() == is_playing { games.data!.score = max }
			else { games.data!.score = min }
		}
	
	}
}
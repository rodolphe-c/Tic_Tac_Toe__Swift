// Copyright © 2016 Rodolphe Cargnello, rodolphe.cargnello@gmail.com

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import Foundation

// Copyable protocol

protocol Copyable { init(instance: Self) }

extension Copyable {
	func copy() -> Self {
		return Self.init(instance: self)
	}
}

// Tile can be empty || x || o

enum Tile : CustomStringConvertible{
	case Empty,O,X
	
	var description : String {
		switch self {
			case .Empty: return " ";
			case .O: return "O";
			case .X: return "X";
		}
	}
}

// TicTacToe Exception

enum TicTacToeError: ErrorType { case InvalidSelection(error: String) }


// TicTacToe

class TicTacToe : CustomStringConvertible, Copyable {
	private var m_board: [[Tile]]
	private var m_is_playing: Tile
	private var m_winner: Tile
	private let m_log_first_player: Tile
	private var m_log_plays : Array<(Int, Int)>
	
	// Constructor
	
	init(){
		m_board =   [
			[Tile.Empty,Tile.Empty,Tile.Empty],
			[Tile.Empty,Tile.Empty,Tile.Empty],
			[Tile.Empty,Tile.Empty,Tile.Empty]
		]
		m_is_playing = Int(arc4random_uniform(100)) % 2 == 0 ? Tile.X : Tile.O
		m_winner = Tile.Empty
		m_log_first_player = Tile.Empty
		m_log_plays = []
	}
	
	required init(instance: TicTacToe) {
		self.m_board = instance.m_board
		self.m_is_playing = instance.isPlaying()
		self.m_winner = instance.winner()
		self.m_log_plays = instance.logPlays()
		self.m_log_first_player = instance.logFirstPlayer()
	}
	
	init(array: [[Tile]]){
		m_board = array
		m_is_playing = Tile.Empty
		m_winner = Tile.Empty
		m_log_first_player = Tile.Empty
		m_log_plays = []
	}
	
	// TicTacToe description 
	
	var description:String {
		var s = "   | 0 | 1 | 2 |\n"
		for i in 0...2
		{
			s+="   -------------\n"
			s+="\(i). "
			for j in 0...2
			{
				s+="| "+m_board[i][j].description+" "
			}
			s+="|\n"
		}
		s+="   -------------\n"
		return s
	}
	
	// Getters
	
	func board() -> [[Tile]] { return m_board }
	func isPlaying() -> Tile { return m_is_playing }
	func logFirstPlayer() -> Tile { return m_log_first_player }
	func logPlays() -> Array<(Int, Int)> { return m_log_plays }
	func winner() -> Tile { return m_winner }
	
	// Play & Run
	
	func play(p: (i: Int, j: Int)) throws -> Bool {
		if m_board[p.i][p.j] == Tile.Empty {
			m_board[p.i][p.j] = m_is_playing;
			
			m_log_plays.insert((p.i,p.j), atIndex: m_log_plays.endIndex)
			
			if m_board[p.i][0] == m_is_playing && m_board[p.i][1] == m_is_playing && m_board[p.i][2] == m_is_playing{
				m_winner = m_is_playing
			}else if m_board[0][p.j] == m_is_playing && m_board[1][p.j] == m_is_playing && m_board[2][p.j] == m_is_playing {
				m_winner = m_is_playing
			}else if m_board[0][0] == m_is_playing && m_board[1][1] == m_is_playing && m_board[2][2] == m_is_playing {
				m_winner = m_is_playing
			}else if m_board[0][2] == m_is_playing && m_board[1][1] == m_is_playing && m_board[2][0] == m_is_playing {
				m_winner = m_is_playing
			}
			
			changePlayer();
			
			return true;
		}
		else
		{
			throw TicTacToeError.InvalidSelection(error: "ERROR: TicTacToe.play(\(p.i), \(p.j))")
		}
	}
	
	func run(player0: Player, player1: Player){
		print("TicTacToe.run(player_0: Player, player_1: Player)\n");
		do {
			try run_(player0, player1: player1)
		}
		catch TicTacToeError.InvalidSelection(let error){
			print(error)
		}
		catch PlayerError.InvalidSelection(let error) {
			print(error)
		}
		catch { }
	}
	
	func isEnded() -> Bool{
		if winner() != Tile.Empty { return true }
		
		for i in 0...2{
			for j in 0...2{
				if m_board[i][j] == Tile.Empty { return false }
			}
		}
		return true
	}
	
	func movesPossible() -> Array<(Int, Int)>{
		var r = Array<(Int, Int)>()
		
		if (isEnded()) { return r }
		for i in 0...2{
			for j in 0...2{
				if m_board[i][j] == Tile.Empty { r.insert((i,j), atIndex: r.endIndex)}
			}
		}
		return r;
	}
	
	private func changePlayer(){
		if m_is_playing == Tile.X {
			m_is_playing = Tile.O;
		}else {
			m_is_playing = Tile.X;
		}
	}
	
	private func run_(player0: Player,player1: Player) throws{
		print(self);
	
		var i = 0
		while true {
			print( "Player \( i % 2) [" + m_is_playing.description + "] plays ");
			
			if i % 2 == 0 {
				while (try play(player0.play(self)) == false) { }
			}
			else {
				while (try play(player1.play(self)) == false) { }
			}
			
			print(self);
			
			if (isEnded()) { break; }
			i+=1
		}
	
		if winner() != Tile.Empty{
			print("Winner is " + m_winner.description+"\n");
		}
		else
		{
			print("No winner\n");
		}
	}
}

func ==(lhs: TicTacToe, rhs: TicTacToe) -> Bool{
	for i in 0...2{
		for j in 0...2{
			if lhs.board()[i][j] != rhs.board()[i][j]{
				return false;
			}
		}
		
	}
	return true;
}